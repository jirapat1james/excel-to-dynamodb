import AWS from 'aws-sdk';

const { IS_OFFLINE } = process.env;

const getExpressionForOperation = (operation, fieldName, query) => {
  const expression = {
    expr: '',
    attr: {},
    name: {},
  };
  const re = RegExp('\\.', 'g');

  switch (operation) {
    case 'gt': {
      const threshold = query;
      const thresholdAttr = `:attr_${fieldName.replace(re, '_')}_threshold`;
      const expr = `${fieldName}>${thresholdAttr}`;

      expression.attr[thresholdAttr] = threshold;
      expression.expr = expr;
      break;
    }
    case 'lt': {
      const threshold = query;
      const thresholdAttr = `:attr_${fieldName.replace(re, '_')}_threshold`;
      const expr = `${fieldName}<${thresholdAttr}`;

      expression.attr[thresholdAttr] = threshold;
      expression.expr = expr;
      break;
    }
    case 'between': {
      const {
        min,
        max,
      } = query;
      const maxAttr = `:attr_${fieldName.replace(re, '_')}_max`;
      const minAttr = `:attr_${fieldName.replace(re, '_')}_min`;
      const attr = {};
      attr[maxAttr] = max;
      attr[minAttr] = min;
      const expr = `${fieldName} BETWEEN ${minAttr} AND ${maxAttr}`;

      expression.attr = attr;
      expression.expr = expr;
      break;
    }
    case 'append_list': {
      const threshold = query;
      const thresholdAttr = `:attr_${fieldName.replace(re, '_')}_threshold`;
      const nameAttr = `#${fieldName}`;
      const expr = `${nameAttr} = list_append(${nameAttr}, ${thresholdAttr})`;

      expression.name[nameAttr] = fieldName;
      expression.attr[thresholdAttr] = [threshold];
      expression.expr = expr;
      break;
    }
    default:
      // statements_def
      break;
  }

  return expression;
};

const getExpression = (update, parent, action) => {
  const result = {};
  const re = RegExp('\\.', 'g');
  result.expr = '';
  result.attr = {};
  result.name = {};
  Object.keys(update).forEach((key) => {
    const o = update[key];
    let keyName;
    if (parent) {
      keyName = `${parent}.${key}`;
    } else {
      keyName = key;
    }

    if (key.startsWith('$')) {
      // Special condition found
      const operation = key.replace('$', '');
      const objResult = getExpressionForOperation(operation, parent, o);
      result.expr += `${objResult.expr}`;
      result.attr = {
        ...result.attr,
        ...objResult.attr,
      };
      result.name = {
        ...result.name,
        ...objResult.name,
      };
    } else {
      let combinationString = 'and';
      if (action === 'update') {
        combinationString = ',';
      }
      if (result.expr !== '') result.expr += ` ${combinationString} `;
      if (typeof o === 'object') {
        if (Array.isArray(o) && action === 'update') {
          if (keyName.indexOf('.') === -1) {
            const nameAttr = `#${keyName}`;
            result.expr += (`${nameAttr}=:attr_${keyName.replace(re, '_')}`);
            result.attr[`:attr_${keyName.replace(re, '_')}`] = o;
            result.name[nameAttr] = keyName;
          } else {
            result.expr += (`${keyName}=:attr_${keyName.replace(re, '_')}`);
            result.attr[`:attr_${keyName.replace(re, '_')}`] = o;
          }
        } else if (Object.keys(o).length === 0) {
          result.expr += (`${keyName}=:attr_${keyName.replace(re, '_')}`);
          result.attr[`:attr_${keyName.replace(re, '_')}`] = {};
        } else {
          const objResult = getExpression(o, keyName, action);
          result.expr += objResult.expr;
          result.attr = { ...result.attr, ...objResult.attr };
          result.name = { ...result.name, ...objResult.name };
        }
      } else if (action === 'update' && keyName.indexOf('.') === -1) {
        const nameAttr = `#${keyName}`;
        result.expr += (`${nameAttr}=:attr_${keyName.replace(re, '_')}`);
        result.attr[`:attr_${keyName.replace(re, '_')}`] = o;
        result.name[nameAttr] = keyName;
      } else {
        result.expr += (`${keyName}=:attr_${keyName.replace(re, '_')}`);
        result.attr[`:attr_${keyName.replace(re, '_')}`] = o;
      }
    }
  });
  return result;
};

/*
  Only supports single expression
 */
const getFilterExpression = (statement, prefix = '') => {
  const conditions = ['<>', '>=', '<=', '>', '<', '='];
  let filterExpression = {};
  let statementCondition;
  for (let i = 0, length1 = conditions.length; i < length1; i++) {
    const condition = conditions[i];
    if (statement.includes((condition))) {
      statementCondition = condition;
      break;
    }
  }

  const field = `${prefix}${statement.split(statementCondition)[0].trim().replace(/"/g, '')}`;
  let value = statement.split(statementCondition)[1].trim();

  const expressionAttributeNames = {};
  const expressionAttributeValues = {};

  expressionAttributeNames[`#${field}`] = field;

  if (statementCondition === '=' && (value[0] === '[' && value[value.length - 1] === ']')) {
    try {
      value = JSON.parse(value.replace(/'/g, '"'));
    } catch (e) {
      throw new Error('value must be json format');
    }
    if (value.length > 0) {
      filterExpression = {
        FilterExpression: `${field} IN (${value.map(v => `:attr_${v}`).join(', ')})`,
        ExpressionAttributeValues: Object.assign({}, ...value.map(v => ({ [`:attr_${v}`]: v }))),
      };
    }
  } else {
    value = value.replace(/"/g, '');
    expressionAttributeNames[`#${field}`] = field;
    expressionAttributeValues[`:attr_${value}`] = value;
    filterExpression = {
      FilterExpression: `#${field} ${statementCondition} :attr_${value}`,
      ExpressionAttributeNames: expressionAttributeNames,
      ExpressionAttributeValues: expressionAttributeValues,
    };
  }

  return filterExpression;
};

export default class dbEngine {
  constructor() {
    if (IS_OFFLINE === 'true') {
      this.dynamoDb = new AWS.DynamoDB.DocumentClient({
        region: 'localhost',
        endpoint: 'http://localhost:8000',
      });
    } else {
      this.dynamoDb = new AWS.DynamoDB.DocumentClient(
        {
          region: 'ap-southeast-1'
        }
      );
    }
  }

  static expr(tableName, indexName, query, action) {
    const cleanedQuery = { ...query };
    const pagination = { ...cleanedQuery.$pagination };
    const filter = { ...query.$filter };
    const fields = { ...query.$fields };

    delete cleanedQuery.$pagination;

    const queryDynamo = getExpression(cleanedQuery, undefined, action);
    const params = {
      TableName: tableName,
      KeyConditionExpression: queryDynamo.expr,
      ExpressionAttributeValues: queryDynamo.attr,
    };

    // Add index name
    if (indexName !== undefined && indexName.trim() !== '') {
      params.IndexName = indexName;
    }

    // Add pagination
    if (pagination !== undefined) {
      const { limit, lastPage } = pagination;
      if (typeof limit === 'number') {
        params.Limit = limit;
      }
      if (lastPage !== undefined) {
        params.ExclusiveStartKey = lastPage;
      }
    }

    if (typeof filter !== 'undefined' && filter.statement) {
      const filterExpressionObj = getFilterExpression(filter.statement, filter.prefix);
      params.ExpressionAttributeValues = {
        ...params.ExpressionAttributeValues,
        ...filterExpressionObj.ExpressionAttributeValues,
      };
      params.ExpressionAttributeNames = {
        ...params.ExpressionAttributeNames,
        ...filterExpressionObj.ExpressionAttributeNames,
      };
      params.FilterExpression = filterExpressionObj.FilterExpression;
      if (Object.keys(params.ExpressionAttributeNames).length === 0) {
        delete params.ExpressionAttributeNames;
      }
    }

    if (fields && fields.length > 0) {
      params.ProjectionExpression = fields.join(',');
    }

    return params;
  }

  /**
   * Filters the result by deleted_flag = Y
   */
  get(tableName, indexName, immutableQuery) {
    return new Promise((resolve, reject) => {
      let fields;
      const query = {
        ...immutableQuery,
      };
      if (query && query.$fields) {
        fields = query.$fields ? `${[...query.$fields].join(',')}, deleted_flag` : undefined;
      }

      if (indexName === undefined || indexName === '') {
        const params = {
          TableName: tableName,
        };

        if (fields) {
          delete query.$fields;
          params.ProjectionExpression = fields;
        }

        params.Key = query;

        this.dynamoDb.get(params).promise()
          .then((result) => {
            const data = { Items: [], Count: 0 };
            if (result.Item) {
              const item = { ...result.Item };

              data.Items.push(item);
              data.Count = 1;
            }
            return resolve(data);
          })
          .catch((error) => {
            // console.error('Error fetching data from dynamodb');
            // console.error(error.message);
            return reject(error);
          });
      } else if (indexName === 'ALL') {
        const params = {
          TableName: tableName,
        };
        // console.log('Scanning table to return all records');
        // console.log(JSON.stringify(params));
        this.dynamoDb.scan(params).promise()
          .then((result) => {
            // console.log(`Dynamodb: result queryAll = ${result}`);
            resolve(result);
          })
          .catch((e) => {
            console.error('Error scanning db');
            console.error(e.message);
          });
      } else {
        const filterDeletedQuery = { ...query };
        const pagination = { ...query.$pagination };
        const sort = { ...query.$sort };
        const filter = { ...query.$filter };

        delete filterDeletedQuery.$pagination;
        delete filterDeletedQuery.$sort;
        delete filterDeletedQuery.$filter;
        delete filterDeletedQuery.$fields;

        const queryDynamo = getExpression(filterDeletedQuery);

        const params = {
          TableName: tableName,
          IndexName: indexName,
          KeyConditionExpression: queryDynamo.expr,
          ExpressionAttributeValues: queryDynamo.attr,
        };
        if (indexName === 'custom-key-condition-expression') {
          params.KeyConditionExpression = query.KeyConditionExpression;
          params.ExpressionAttributeValues = query.ExpressionAttributeValues;
          params.IndexName = query.IndexName;
        }
        if (typeof pagination !== 'undefined') {
          params.Limit = pagination.limit;
          if (pagination.lastEvaluatedKey) {
            params.ExclusiveStartKey = pagination.lastEvaluatedKey;
          }
        }

        if (typeof sort !== 'undefined' && sort.direction) {
          if (sort.direction.toLowerCase() === 'descending') {
            params.ScanIndexForward = false;
          }
        }

        if (typeof filter !== 'undefined' && filter.statement) {
          const filterExpressionObj = getFilterExpression(filter.statement, filter.prefix);
          params.ExpressionAttributeValues = {
            ...params.ExpressionAttributeValues,
            ...filterExpressionObj.ExpressionAttributeValues,
          };
          params.ExpressionAttributeNames = {
            ...params.ExpressionAttributeNames,
            ...filterExpressionObj.ExpressionAttributeNames,
          };
          params.FilterExpression = filterExpressionObj.FilterExpression;

          if (Object.keys(params.ExpressionAttributeNames).length === 0) {
            delete params.ExpressionAttributeNames;
          }
        }

        if (fields && fields.length > 0) {
          params.ProjectionExpression = fields;
        }
        this.dynamoDb.query(params).promise()
          .then((result) => {
            if (result) {
              const mutableResult = { ...result };
              let { Items: items } = mutableResult;
              // items = items.filter(item => item.deleted_flag === 'N');

              items = items.map((item) => {
                const mutableItem = { ...item };

                return mutableItem;
              });

              mutableResult.Items = items;
              return resolve(mutableResult);
            }
            return resolve({ Items: [] });
          })
          .catch((e) => {
            // console.error('Error querying database');
            // console.error(e.message);
            reject(e);
          });
      }
    });
  }

  /*
   * Actually gets everything without checking for deleted
   * flag
   */
  truelyGet(tableName, indexName, query) {
    return new Promise((resolve, reject) => {
      if (indexName === undefined || indexName === '') {
        const params = {
          TableName: tableName,
          Key: query,
        };
        this.dynamoDb.get(params).promise()
          .then((result) => {
            const data = { Items: [], Count: 0 };
            if (result.Item) {
              data.Items.push(result.Item);
              data.Count = 1;
            }
            return resolve(data);
          })
          .catch((e) => {
            console.log(`Error fetching: ${e.message}`);
            return reject(e);
          });
      } else if (indexName === 'ALL') {
        const params = {
          TableName: tableName,
        };
        this.dynamoDb.scan(params).promise()
          .then(result => resolve(result))
          .catch(e => reject(e));
      } else {
        const queryDynamo = getExpression(query);
        const params = {
          TableName: tableName,
          IndexName: indexName,
          KeyConditionExpression: queryDynamo.expr,
          ExpressionAttributeValues: queryDynamo.attr,
        };

        this.dynamoDb.query(params).promise()
          .then(result => resolve(result))
          .catch(e => reject(e));
      }
    });
  }

  insert(tableName, item) {
    return new Promise((resolve, reject) => {
      const itemToInsert = { ...item };
      if (!item.deleted_flag) {
        itemToInsert.deleted_flag = 'N';
      }

      const params = {
        TableName: tableName,
        Item: itemToInsert,
      };
      this.dynamoDb.put(params).promise()
        .then(result => resolve(result))
        .catch(e => reject(e));
    });
  }

  update(tableName, query, update) {
    return new Promise((resolve, reject) => {
      const processedUpdate = { ...update };

      const updateDynamo = getExpression(processedUpdate, undefined, 'update');
      const params = {
        TableName: tableName,
        Key: query,
        UpdateExpression: `set ${updateDynamo.expr}`,
        ExpressionAttributeValues: updateDynamo.attr,
        ExpressionAttributeNames: updateDynamo.name,
        ReturnValues: 'ALL_NEW',
      };
      // console.log(`update expression ${params.UpdateExpression}`);
      if (Object.keys(params.ExpressionAttributeNames).length === 0) {
        delete params.ExpressionAttributeNames;
      }

      this.dynamoDb.update(params).promise()
        .then(result => resolve(result))
        .catch((e) => {
          if (e.message.includes('The document path provided in the update expression is invalid for update')) {
            console.log('Got error trying to update. Might be because new mapped fields are being added');
            const newParams = {
              ...params,
              UpdateExpression: `put ${updateDynamo.expr}`,
            };
            this.dynamoDb.update(newParams).promise()
              .then(result => resolve(result))
              .catch(error => reject(error));
          } else {
            reject(e);
          }
        });
    });
  }

  /**
   * Does not actually delete. Just sets the deleted_flag to Y
   */
  delete(tableName, query) {
    return new Promise((resolve, reject) => {
      const params = {
        TableName: tableName,
        Key: query,
      };
      this.dynamoDb.delete(params).promise()
        .then(result => resolve(result))
        .catch(e => reject(e));
    });
  }

  /**
   * Actually deleted data from the db
   */
  dangerouslyDelete(tableName, query) {
    return new Promise((resolve, reject) => {
      const params = {
        TableName: tableName,
        Key: query,
      };
      this.dynamoDb.delete(params).promise()
        .then(result => resolve(result))
        .catch(e => reject(e));
    });
  }
}
