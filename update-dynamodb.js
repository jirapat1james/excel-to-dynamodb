var Excel  = require ('exceljs');
var uuid = require('uuid');
var workbook = new Excel.Workbook();
import { sleep } from './alfred';
import moment, { months } from 'moment';
import Models from './app';
import DBEngine from './dbengine/dbengine.js';

const dbengine = new DBEngine();

const {
  ResponseModel,
} = Models;

const RESPONSE_TABLE = 'core-atv-response-summary-table-staging';

const appResponseSummary = new ResponseModel(dbengine, RESPONSE_TABLE);

let results;
var now = moment().utcOffset('+0700').unix();
console.log(now);
new Promise(async (resolve,reject) => {
  try {
    results = await appResponseSummary.list('quota-uvs-sales',now);
  } catch (e) {
    reject(e);
  }
  const filterResults = results.filter(result => result.period_end === 1585699200);
  console.log(filterResults);
  let updateResults = [];
  for (let i = 0; i < filterResults.length; i++) {
    const toUpdateId = filterResults[i].response_id;
    let updateResult;
    try{
      updateResult = await appResponseSummary.update(
        toUpdateId,
        undefined,
        undefined,
        undefined,
        1583020800,
        1584144000
      );
      updateResults.push(updateResult);
    } catch (e) {
      reject(e)
    }
  }
  console.log(updateResults);
  resolve();
});


