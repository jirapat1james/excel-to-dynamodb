var Excel  = require ('exceljs');
var uuid = require('uuid');
var workbook = new Excel.Workbook();
import { sleep } from './alfred';
import moment, { months } from 'moment';
import Models from './app';
import DBEngine from './dbengine/dbengine.js';

const dbengine = new DBEngine();

const {
  ResponseModel,
} = Models;

const RESPONSE_TABLE = 'core-atv-response-summary-table-staging';

const appResponseSummary = new ResponseModel(dbengine, RESPONSE_TABLE);

let results;
var now = moment().utcOffset('+0700').unix();
console.log(now);
new Promise(async (resolve,reject) => {
  try {
    results = await appResponseSummary.list('quota-uvs-sales-w2-2020',now);
  } catch (e) {
    reject(e);
  }
  const filterResults = results.filter(result => result.period_end > 0);
  console.log(filterResults);
  for (let i = 0; i < filterResults.length; i++) {
    const toDeleteId = filterResults[i].response_id;
    try{
      await appResponseSummary.dangerouslyDelete(toDeleteId);
    } catch (e) {
      reject(e)
    }
  }
  resolve();
});


