import ResponseModel from './model/responseModel';
import RespondentDetailModel from './model/respondentDetailModel';
const app = {
  ResponseModel,
  RespondentDetailModel,
};

module.exports = app;
