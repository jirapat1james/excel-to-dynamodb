var Excel  = require ('exceljs');
var uuid = require('uuid');
var workbook = new Excel.Workbook();
import { sleep } from './alfred';
import moment, { months } from 'moment';
import Models from './app';
import DBEngine from './dbengine/dbengine.js';

const dbengine = new DBEngine();

const {
  RespondentDetailModel,
} = Models;

const RESPONDENT_DETAIL_TABLE = 'core-atv-respondent-detail-table-prd';

const appRespondentDetail = new RespondentDetailModel(dbengine, RESPONDENT_DETAIL_TABLE);

workbook.xlsx.readFile('updateList.xlsx')
    .then(function() {
        var worksheet = workbook.getWorksheet(1);
        var key = [];
        var value = [];
        worksheet.eachRow(async function(row, rowNumber) {
          if(rowNumber===1) {
            var rowArr = row.values;
            key = rowArr.filter(function (el) {
              return el != null;
            });
          } else {
            var rowArr = row.values;
            value = rowArr.filter(function (el) {
              return el != null;
            });
          }
          if(key.length >0 && value.length > 0) {
            if (9001 < rowNumber && rowNumber < 10000) {
              //read rdId from excel
              var rdId = value[0];
              var response_id = "quota-uvs-sales-w2-2020|"+value[1]+"|"+value[2];
              var quota_id = "quota-uvs-sales-w2-2020"
              // console.log(rdId);
              // update
              var update = await appRespondentDetail.updateResponseId(rdId,response_id,quota_id);
              console.log(update);
            }
          }
      });
    }).catch (function(e) {
      console.log('FAIL : rdId ='+rdId);
    });


