/**
 * Contains all business logic for Respondent. Can be used in handler
 * to respond to requests
 * @access public
 */
const uuidv4 = require('uuid/v4');

export default class RespondentDetail {
  /**
   * To construct an instance of Respondent, you'll need to provide it a DBEngine.
   * @param {Object} db The DBEngine to be used to interact with data.
   * @param {!string} tableName - The name of the table to query the data from
   */
  constructor(db, tableName) {
    this.db = db;
    this.tableName = tableName;
    this.modelName = 'RespondentDetail';
  }
  static deepEqual(x, y) {
    const ok = Object.keys;
    const tx = typeof x;
    const ty = typeof y;
    return x && y && tx === 'object' && tx === ty ? (
      ok(x).length === ok(y).length &&
        ok(x).every(key => RespondentDetail.deepEqual(x[key], y[key]))
    ) : (x === y);
  }

  /**
   * Use this function to fetch a single record from the database.
   * @param {!string} rdId - The rd_id you want to query
   * @throws {Error} if instance does not have a db
   * @throws {Error} if instance does not have a tableName
   * @throws {Error} if rd_id param is null or undefined
   * @throws {Error} if rd_id param is wrong type
   * @throws {Error} if query failed
   */
  get(rdId) {
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        reject(new Error(`Query ${this.modelName} Failed. No database set for Instance`));
      } else if (!this.tableName) {
        reject(new Error(`Query ${this.modelName} Failed. No table set for Instance`));
      } else if (!rdId || rdId === '') {
        reject(new Error(`Query ${this.modelName} Failed. RD ID is not provided`));
      } else if (rdId && typeof rdId !== 'string') {
        reject(new Error(`Query ${this.modelName} Failed. Type of RD ID is wrong`));
      } else {
        const indexName = `${this.tableName}-index-rd_id-created_date`;
        const query = {
          rd_id: rdId,
          $pagination: {
            limit: 1,
          },
          $sort: {
            direction: 'descending',
          },
        };
        try {
          const results = await this.db.get(this.tableName, indexName, query);
          const items = results.Items;
          let activeResults = [];
          if (items.length > 0) {
            activeResults = items.filter(item => item.deleted_flag === 'N');
          }
          resolve(activeResults);
        } catch (error) {
          if (error.message.includes('Cannot read from backfilling global secondary index')) {
            const oldIndexName = `${this.tableName}-index-rd_id`;
            const oldQuery = {
              rd_id: rdId,
            };
            try {
              const results = await this.db.get(this.tableName, oldIndexName, oldQuery);
              const items = results.Items;
              let activeResults = [];
              if (items.length > 0) {
                activeResults = items.filter(item => item.deleted_flag === 'N');
              }
              return resolve(activeResults);
            } catch (e) {
              // console.log(e);
              return reject(new Error(`Query ${this.modelName} Failed. ${e}`));
            }
          }
          // console.log(error);
          reject(new Error(`Query ${this.modelName} Failed. ${error}`));
        }
      }
    });
  }

  /**
   * Use this function to insert new record to the database.
   * @param {!item} item - The object of the record you want to insert
   * @throws {Error} if instance does not have a db
   * @throws {Error} if instance does not have a tableName
   * @throws {Error} if item param is null or undefined
   * @throws {Error} if item param deployment id is null or undefined
   * @throws {Error} if item param deployment id is wrong type
   * @throws {Error} if item param id cannot be provided
   * @throws {Error} if item param already have deployment
   * @throws {Error} if insert failed
   */
  insert(item) {
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        reject(new Error(`Insert ${this.modelName} Failed. No database set for Instance`));
      } else if (!this.tableName) {
        reject(new Error(`Insert ${this.modelName} Failed. No table set for Instance`));
      } else if (!item) {
        reject(new Error(`Insert ${this.modelName} Failed. Items is not provided`));
      } else if (!item.rd_id || item.rd_id === '') {
        reject(new Error(`Insert ${this.modelName} Failed. Items RD ID is not provided`));
      } else if (typeof item.rd_id !== 'string') {
        reject(new Error(`Insert ${this.modelName} Failed. Type of RD ID is wrong`));
      } else if (item.id) {
        reject(new Error(`Insert ${this.modelName} Failed. Items ID can not be provided`));
      } else {
        const date = new Date();
        const newItem = { ...item };
        if (!newItem.id) newItem.id = uuidv4();
        newItem.created_date = date.toISOString();
        newItem.deleted_flag = 'N';
        newItem.deleted_date = '-';
        try {
          await this.db.insert(this.tableName, newItem);
          resolve(newItem);
        } catch (error) {
          console.log(error);
          reject(new Error(`Insert ${this.modelName} Failed. ${error}`));
        }
      }
    });
  }

  /**
   * Use this function to update record to the database.
   * @param {!string} rdId - The rd_id you want to query
   * @param {!string} deleteFlag - The type of deletion you want to delete
   * @throws {Error} if instance does not have a db
   * @throws {Error} if instance does not have a tableName
   * @throws {Error} if param rd_id is null or undefined
   * @throws {Error} if param rd_id is wrong type
   * @throws {Error} if deleteFlag param is not correct
   * @throws {Error} if record of rd_id not found
   * @throws {Error} if delete failed
   */
  delete(rdId, deleteFlag) {
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        reject(new Error(`Delete ${this.modelName} Failed. No database set for Instance`));
      } else if (!this.tableName) {
        reject(new Error(`Delete ${this.modelName} Failed. No table set for Instance`));
      } else if (!rdId || rdId === '') {
        reject(new Error(`Delete ${this.modelName} Failed. RD ID is not provided`));
      } else if (typeof rdId !== 'string') {
        reject(new Error(`Delete ${this.modelName} Failed. Type of RD ID is wrong`));
      } else if (deleteFlag && deleteFlag !== 'Y' && deleteFlag !== 'R') {
        reject(new Error(`Delete ${this.modelName} Failed. deleteFlag must be Y or R`));
      } else {
        const date = new Date();
        const deleted = {
          deleted_date: date.toISOString(),
          deleted_flag: (!deleteFlag) ? 'Y' : deleteFlag,
        };

        await this.get(rdId).then((results) => {
          if (results.length === 0) {
            reject(new Error(`Delete ${this.modelName} Failed. record for RD ID = ${rdId} not found`));
          } else {
            const deleteItem = results.map(result => new Promise(async (resolveItem, rejectItem) => {
              const query = {
                id: result.id,
              };
              try {
                await this.db.update(this.tableName, query, deleted);
                return resolveItem({ message: 'Delete successfully' });
              } catch (error) {
                console.log(error);
                return rejectItem(error);
              }
            }));

            Promise.all(deleteItem)
              .then((deleteResult, errorResult) => {
                resolve(deleteResult);
                reject(new Error(`Delete ${this.modelName} Failed. ${errorResult}`));
              })
              .catch((error) => {
                reject(new Error(`Delete ${this.modelName} Failed. ${error}`));
              });
          }
        });
      }
    });
  }

  /**
   * Use this function to delete record to the database.
   * @param {!string} rid - The id you want to query
   * @throws {Error} if instance does not have a db
   * @throws {Error} if instance does not have a tableName
   * @throws {Error} if param id is null or undefined
   * @throws {Error} if param id is wrong type
   * @throws {Error} if record of id not found
   * @throws {Error} if delete failed
   */
  dangerouslyDelete(id) {
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        reject(new Error(`Delete ${this.modelName} Failed. No database set for Instance`));
      } else if (!this.tableName) {
        reject(new Error(`Delete ${this.modelName} Failed. No table set for Instance`));
      } else if (!id || id === '') {
        reject(new Error(`Delete ${this.modelName} Failed. ID is not provided`));
      } else if (typeof id !== 'string') {
        reject(new Error(`Delete ${this.modelName} Failed. Type of ID is wrong`));
      } else {
        try {
          await this.db.dangerouslyDelete(this.tableName, { id });
          return resolve({ message: 'Dangerously Delete successfully' });
        } catch (error) {
          console.log(error);
          return reject(error);
        }
      }
    });
  }

  /**
   * Use this function to update record to the database.
   * @param {!id} rdId - The rd_id you want to query
   * @param {!updated} item - The object of the record you want to update
   * @throws {Error} if instance does not have a db
   * @throws {Error} if instance does not have a tableName
   * @throws {Error} if param rd_id is null or undefined
   * @throws {Error} if param item is null or undefined
   * @throws {Error} if param rd_id is wrong type
   * @throws {Error} if param item is wrong type
   * @throws {Error} if record of deployment_id not found
   * @throws {Error} if update fail
   */
  updateResponseId(rdId,response_id,quota_id) {
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        reject(new Error(`Update ${this.modelName} Failed. No database set for Instance`));
      } else if (!this.tableName) {
        reject(new Error(`Update ${this.modelName} Failed. No table set for Instance`));
      } else if (!rdId) {
        reject(new Error(`Update ${this.modelName} Failed. RD ID is not provided`));
      } else if (typeof rdId !== 'string') {
        reject(new Error(`Update ${this.modelName} Failed. Type of RD ID is wrong`));
      } else {
        await this.get(rdId).then((results) => {
          if (results.length === 0) {
            reject(new Error(`Update ${this.modelName} Failed. record for RD ID = ${rdId} not found`));
          } else {
            const updateItem = results.map(result => new Promise(async (resolveItem, rejectItem) => {
              const query = {
                id: result.id,
              };
              try {
                await this.db.update(this.tableName, query, { response_id, quota_id });
                return resolveItem({ message: 'Update successfully '+rdId});
              } catch (error) {
                console.log(error);
                return rejectItem(error);
              }
            }));

            Promise.all(updateItem)
              .then((updateResult, errorResult) => {
                resolve(updateResult);
                reject(new Error(`Delete ${this.modelName} Failed. ${errorResult}`));
              })
              .catch((error) => {
                reject(new Error(`Delete ${this.modelName} Failed. ${error}`));
              });
          }
        });
      }
    });
  }

  /**
   * Use this function to update record to the database.
   * @param {!updated} item - The object of the record you want to update
   * @throws {Error} if instance does not have a db
   * @throws {Error} if instance does not have a tableName
   * @throws {Error} if item param is null or undefined
   * @throws {Error} if item param is wrong type
   * @throws {Error} if item rd_id param is null or undefined
   * @throws {Error} if item rd_id param is wrong type
   * @throws {Error} if replace fail
   */
  replace(item) {
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        reject(new Error(`Replace ${this.modelName} Failed. No database set for Instance`));
      } else if (!this.tableName) {
        reject(new Error(`Replace ${this.modelName} Failed. No table set for Instance`));
      } else if (!item) {
        reject(new Error(`Replace ${this.modelName} Failed. Item is not provided`));
      } else if (typeof item !== 'object') {
        reject(new Error(`Replace ${this.modelName} Failed. Type of item is wrong`));
      } else if (!item.rd_id) {
        reject(new Error(`Replace ${this.modelName} Failed. Item RD ID is not provided`));
      } else if (typeof item.rd_id !== 'string') {
        reject(new Error(`Replace ${this.modelName} Failed. Type of RD ID is wrong`));
      } else {
        await this.get(item.rd_id).then((results) => {
          Promise.all(results.map(deleteItem => this.delete(deleteItem.rd_id, 'R')))
            .then(() => {
              const newItem = { ...item };
              newItem.id = '';
              return this.insert(newItem);
            })
            .then(updatedResult => resolve(updatedResult))
            .catch((error) => {
              console.log(error);
              reject(new Error(`Replace ${this.modelName} Failed. ${error}`));
            });
        });
      }
    });
  }
}
