import uuid from 'uuid';
// import Logger from 'logger';
import moment from 'moment';
// import blunder from '../utils/blunder';

console.log('reachModel');
const MODEL_NAME = 'Response';
// const logger = new Logger('ResponseModel');

export default class ResponseModel {
  /**
   * @param  {Object} An instance of dbEngine
   * @param  {string} The table name you want to operate on
   */
  constructor(db,tableName) {
    this.db = db;
    this.tableName = tableName;
    this.modelName = MODEL_NAME;
  }
  
  /**
   * Fetches and returns a single response counter for a quota
   * @param  {string} response_id              The response's id you want response for
   * @return {Promise<Error, response>}
   */
  get(responseId) {
    return new Promise(async (resolve, reject) => {
      const logSource = 'get';
      if (!this.db) {
        return reject(new Error('You did not set dbEngine for the this instance'));
      }
      if (!this.tableName) {
        return reject(new Error('You did not set table name for the this instance'));
      }
      if (!responseId) {
        return reject(new Error(blunder.errors.responseIDMissingResponseGet));
      }
      if (typeof responseId !== 'string') {
        return reject(new Error(blunder.errors.ResponseIdParamNotString));
      }

      const query = {
        response_id: responseId,
      };
      try {
        const result = await this.db.get(this.tableName, undefined, query);
        if (result.Items.length === 0) {
          return reject(new Error(blunder.errors.responseNotFound));
        }
        return resolve(result.Items[0]);
      } catch (error) {
        console.error('Got error fetching response by id', logSource, {
          error: error.message,
        });
        return reject(error);
      }
    });
  }

  /**
   * Fetches and returns list of response for a quota
   * @param  {string} quota_id              The quota's id you want response for
   * @return {Promise<Error, response>}
   */
  list(quotaId, now) {
    return new Promise(async (resolve, reject) => {
      const logSource = 'list';
      if (!this.db) {
        return reject(new Error('You did not set dbEngine for the this instance'));
      }
      if (!this.tableName) {
        return reject(new Error('You did not set table name for the this instance'));
      }
      if (!quotaId) {
        return reject(new Error(blunder.errors.quotaIDMissingResponseGet));
      }
      if (typeof quotaId !== 'string') {
        return reject(new Error(blunder.errors.QuotaIdParamNotString));
      }

      const queryWithPeriodStart = {
        quota_id: quotaId,
        period_start: {
          $lt: now,
        },
      };
      const indexNameWithPeriodStart = `${this.tableName}-index-quota_id-period_start`;
      const queryWithPeriodEnd = {
        quota_id: quotaId,
        period_end: {
          $gt: now,
        },
      };
      const indexNameWithPeriodEnd = `${this.tableName}-index-quota_id-period_end`;
      try {
        const result = await this.db.get(this.tableName, indexNameWithPeriodStart, queryWithPeriodStart);
        const resultItems = result.Items;
        const returnItems = resultItems.filter(resultObj => resultObj.period_end > now);
        // console.log('Get result (from using periodStart Index', logSource, {
          // returnItems,
        // });
        return resolve(returnItems || []);
      } catch (error) {
        console.error('Got error fetching responses by quota', logSource, {
          error: error.message,
        });
        if (error.message.includes('Provision')) {
          try {
            const result = await this.db.get(this.tableName, indexNameWithPeriodEnd, queryWithPeriodEnd);
            const resultItems = result.Items;
            const returnItems = resultItems.filter(resultObj => resultObj.period_start < now);
            // console.log('Get result (from using periodEnd Index', logSource, {
            //   returnItems,
            // });
            return resolve(returnItems || []);
          } catch (newError) {
            console.error('Got error fetching responses by quota', logSource, {
              error: newError.message,
            });
          }
        }
        return reject(error);
      }
    });
  }

  /**
   * Inserts a new response record in db for a quota
   * @param  {string} quota_id   The id of quota to create response for
   * @param  {object} matching_data       Matching data for grouping
   * @param  {object} period_windows      period windows
   * @param  {number} sampling_counter      The number of successful responses
   * @param  {number} number_of_sent_out      The number of successful responses
   * @param  {number} number_of_response      The number of successful responses
   * @return {Promise<Error, response>}
   * @throws {Error} If quota with quotaId is not found
   * @throws {Error} If response number_of_response for this quota already exists
   */
  insert(responseId, quotaId, matchingData, matchingGroupValue, periodWindows, samplingCounter, numberOfSentOut, numberOfResponse) {
    const logSource = 'insert';
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        return reject(new Error('You did not set dbEngine for the this instance'));
      }
      if (!this.tableName) {
        return reject(new Error('You did not set table name for the this instance'));
      }
      if (!quotaId) {
        return reject(new Error(blunder.errors.quotaIDMissingResponseInsert));
      }
      if (typeof quotaId !== 'string') {
        return reject(new Error(blunder.errors.QuotaIdParamNotString));
      }
      if (typeof samplingCounter !== 'number' && samplingCounter !== undefined) {
        return reject(new Error(blunder.errors.samplingCountParamNotNumber));
      }
      if (typeof numberOfSentOut !== 'number' && numberOfSentOut !== undefined) {
        return reject(new Error(blunder.errors.sentOutCountParamNotNumber));
      }
      if (typeof numberOfResponse !== 'number' && numberOfResponse !== undefined) {
        return reject(new Error(blunder.errors.countParamNotNumber));
      }
      if (samplingCounter !== undefined && samplingCounter < 0) {
        return reject(new Error(blunder.errors.samplingCountParamNumberInvalid));
      }
      if (numberOfSentOut !== undefined && numberOfSentOut < 0) {
        return reject(new Error(blunder.errors.sentOutCountParamNumberInvalid));
      }
      if (numberOfResponse !== undefined && numberOfResponse < 0) {
        return reject(new Error(blunder.errors.countParamNumberInvalid));
      }
      if (matchingData) {
        if (typeof matchingData !== 'object') {
          return reject(new Error(blunder.errors.matchingDataIsNotObject));
        }
      }
      if (!periodWindows) {
        return reject(new Error(blunder.errors.periodWindowsMissing));
      }
      if (typeof periodWindows !== 'object') {
        return reject(new Error(blunder.errors.periodWindowsIsNotObject));
      }
      if (
        samplingCounter === undefined &&
        numberOfSentOut === undefined &&
        numberOfResponse === undefined
      ) {
        return reject(new Error(blunder.errors.nothingToUpdate));
      }
      const item = {
        response_id: responseId,
        quota_id: quotaId,
        matching_data: matchingData,
        matching_group_value: matchingGroupValue,
        period_windows: periodWindows,
        sampling_counter: (samplingCounter !== undefined) ? samplingCounter : 1,
        number_of_sent_out: (numberOfSentOut !== undefined) ? numberOfSentOut : 1,
        number_of_response: (numberOfResponse !== undefined) ? numberOfResponse : 0,
      };
      const startTime = moment(periodWindows.start, 'DD/MM/YYYY HH:mm ZZ').unix();
      const endTime = moment(periodWindows.end, 'DD/MM/YYYY HH:mm ZZ').unix();
      console.log('Start/End time index', logSource, {
        start: startTime,
        end: endTime,
      });
      item.period_start = startTime;
      item.period_end = endTime;
      try {
        await this.db.insert(this.tableName, item);
        return resolve(item);
      } catch (error) {
        return reject(error);
      }
    });
  }

  /**
   * Updates the response number_of_response to the new number_of_response.
   * If does not exist, it'll create
   * a new one
   * @param  {string} response_id              The quota's id response is updated for
   * @param  {number} sampling_counter      The sampling counter
   * @param  {number} number_of_sent_out    The number of sent out
   * @param  {number} number_of_response    The number of successful responses
   * @return {Promise<Error, response>}
   */
  update(responseId, samplingCounter, numberOfSentOut, numberOfResponse, periodStart, periodEnd) {
    return new Promise(async (resolve, reject) => {
      if (!this.db) {
        return reject(new Error('You did not set dbEngine for the this instance'));
      }
      if (!this.tableName) {
        return reject(new Error('You did not set table name for the this instance'));
      }

      const toUpdate = {};
      if (samplingCounter !== undefined) toUpdate.sampling_counter = samplingCounter;
      if (numberOfSentOut !== undefined) toUpdate.number_of_sent_out = numberOfSentOut;
      if (numberOfResponse !== undefined) toUpdate.number_of_response = numberOfResponse;
      if (periodStart !== undefined) toUpdate.period_start = periodStart;
      if (periodEnd !== undefined) toUpdate.period_end = periodEnd;
      const query = {
        response_id: responseId,
      };
      try {
        const result = await this.db.update(this.tableName, query, toUpdate);
        const newResponse = {
          ...result.Attributes[0],
          ...toUpdate,
        };
        return resolve(newResponse);
      } catch (err) {
        return reject(err);
      }
    });
  }

  /**
   * Updates the response number_of_response to the new number_of_response.
   * If does not exist, it'll create
   * a new one
   * @param  {string} response_id              The quota's id response is updated for
   * @param  {number} sampling_counter      The number of sampling counter increase, 0 to reset
   * @param  {number} number_of_sent_out    The number of sent out increases
   * @param  {number} number_of_response    The number of successful responses increase
   * @return {Promise<Error, response>}
   */
  async increase(responseId, samplingCounter, numberOfSentOut, numberOfResponse) {
    const logSource = 'increase';
    console.log('Reach increase', logSource, {
      responseId,
      samplingCounter,
      numberOfSentOut,
      numberOfResponse,
    });
    const response = await this.get(responseId);
    let updatedSampling = response.sampling_counter;
    if (samplingCounter !== undefined) {
      updatedSampling = ((samplingCounter === 0) ? 0 : response.sampling_counter + samplingCounter);
    }
    const updatedSentOut =
      (numberOfSentOut) ? response.number_of_sent_out + numberOfSentOut :
        response.number_of_sent_out;
    const updatedResponse =
      (numberOfResponse) ? response.number_of_response + numberOfResponse :
        response.number_of_response;
    console.log('Update with this value', logSource, {
      responseId,
      updatedSampling,
      updatedSentOut,
      updatedResponse,
    });
    return this.update(responseId, updatedSampling, updatedSentOut, updatedResponse);
  }

  dangerouslyDelete(responseId) {
    return new Promise(async (resolve, reject) => {
      if (typeof responseId !== 'string') {
        return reject(new Error('No id in item. Can only delete items with id'));
      }
      const query = {
        response_id: responseId,
      };
      console.log(query);
      try {
        await this.db.delete(this.tableName, query);
        return resolve({
          response_id: responseId,
        });
      } catch (error) {
        return reject(error);
      }
    });
  }
}

console.log('end');
