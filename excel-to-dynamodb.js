var Excel  = require ('exceljs');
var uuid = require('uuid');
var workbook = new Excel.Workbook();
import { sleep } from './alfred';
import Models from './app';
import DBEngine from './dbengine/dbengine.js';

const dbengine = new DBEngine();

const {
  ResponseModel,
} = Models;

const RESPONSE_TABLE = 'core-atv-response-summary-table-prd';

const appResponseSummary = new ResponseModel(dbengine, RESPONSE_TABLE);
// {
//   "deleted_flag": "N",
//   "matching_data": {
//     "location_meta4": "2610400",
//     "wave": "w12020-sale"
//   },
//   "matching_group_value": "2610400|w12020-sale",
//   "number_of_response": 1,
//   "number_of_sent_out": 1,
//   "period_end": 1580515200,
//   "period_start": 1577836800,
//   "period_windows": {
//     "end": "13/06/2020 00:00 +0000",
//     "start": "03/02/2020 00:00 +0000"
//   },
//   "quota_id": "quota-uvs-service-w1-2020",
//   "response_id": "07954ee3-1849-44c5-ba91-6f41a8470f86",
//   "sampling_counter": 0
// }
var defaultItem = {
  deleted_flag: "N",
  matching_data: {},
  matching_group_value: "-",
  number_of_response: "-",
  number_of_sent_out: "-",
  period_windows: {
    "end": "12/09/2020 00:00 +0000",
    "start": "02/04/2020 00:00 +0000"
  },
  quota_id: "quota-uvs-sales-w2-2020",
  response_id: 0,
  sampling_counter: 0
};
workbook.xlsx.readFile('insertList.xlsx')
    .then(function() {
        var worksheet = workbook.getWorksheet(1);
        var key = [];
        var value = [];
        worksheet.eachRow(async function(row, rowNumber) {
          if(rowNumber===1) {
            var rowArr = row.values;
            key = rowArr.filter(function (el) {
              return el != null;
            });
          } else {
            var rowArr = row.values;
            value = rowArr.filter(function (el) {
              return el != null;
            });
          }
          if(key.length >0 && value.length > 0) {
            if (600 < rowNumber && rowNumber < 800) {
              var key0 = key[0].toLowerCase();
              var key1 = key[1].toLowerCase();
              var matching_data = {};
              matching_data[key0] = ''+value[0];
              matching_data[key1] = value[1];
              var number_of_response = value[3];
              var number_of_sent_out = value[2];
              var matching_group_value = value[4];
              var newItem = defaultItem;
              newItem.matching_data = matching_data;
              newItem.number_of_response = number_of_response;
              newItem.number_of_sent_out = number_of_sent_out;
              newItem.matching_group_value = matching_group_value;
              newItem.response_id = 'quota-uvs-sales-w2-2020|'+matching_group_value;
              // console.log(newItem);
              // insert
              var insert = await appResponseSummary.insert(newItem.response_id, newItem.quota_id, newItem.matching_data, newItem.matching_group_value, newItem.period_windows, newItem.sampling_counter, newItem.number_of_sent_out, newItem.number_of_response);
              // console.log(insert);
              await sleep(100);
            }
          }
      });
    }).catch (function(e) {
      console.log(e);
    });

