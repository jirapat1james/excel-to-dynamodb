export const sleep = waitInSeconds => new Promise((resolve) => {
  let mutableWaitInSeconds = waitInSeconds || 50;
  if (Number.isNaN(mutableWaitInSeconds)) {
    // Defaults to 50 seconds
    mutableWaitInSeconds = 50;
  }
  const waitInMilliSeconds = 1000 * mutableWaitInSeconds;
  setTimeout(resolve, waitInMilliSeconds);
});